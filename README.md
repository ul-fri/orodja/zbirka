# Orodja za ravnanje z nalogami in zbirkami nalog


## Namestitev

Namestitev lahko izvedete z ukazom pip

    pip install https://gitlab.com/ul-fri/orodja/zbirka/-/archive/master/zbirka-master.zip

Ali pa prenesete izvorno kodo in poženete setup.py

    git clone https://gitlab.com/ul-fri/orodja/zbirka.git
    cd zbirka
    python3 setup.py install
