from setuptools import setup

description = 'Orodje za upravljanje z zbirkami nalog.'

with open('README.md') as f:
    long_description = f.read()

setup(
    name='zbirka',
    version='0.0.1',
    author='Martin Vuk',
    description=description,
    long_description=long_description,
    license='MIT',
    keywords='latex',
    setup_requires = ['pytest-runner'],
    tests_require = ['pytest'],
    install_requires=['numpy>=1.12.0'],
    packages=['naloge'],
    scripts= ['izlusci_resitve'],
    classifiers=[
        'Intended Audience :: Education',
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: Slovenian',
        'Topic :: Education/Tools ',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.6',
    ],
)