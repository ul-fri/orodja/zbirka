#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import os

REZULTAT_TPL = """\\begin{{rezultatvp}}\\label{{{label} O}}
\\hfill\\hyperref[{label} N]{{\\linkn}}
{rezultat}
\\end{{rezultatvp}}
""" 
RESITEV_TPL = """\\begin{{resitevp}}\\label{{{label} R}}
\\hfill\\hyperref[{{{label} N}}]{{\\linkn}}
{resitev}
\\end{{resitevp}}
""" 
CHAPTER_RE = re.compile(r"\\chapter{(.*?)}")
REZULTAT_RE = re.compile(r"^\s*\\begin{rezultat}(.*?)\\end{rezultat}", re.DOTALL| re.M)
RESITEV_RE = re.compile(r"^\s*\\begin{resitev}(.*?)\s*\\end{resitev}", re.DOTALL| re.M)
NALOGA_RE = re.compile(r"^\s*\\begin{naloga}.*?\\end{naloga}", re.DOTALL| re.M)
KOMENTAR_RE = re.compile(r"%.*")
LABEL_RE = re.compile(r"\\begin{naloga}{(.*?)}")

def izlusci_resitve(tex_datoteka):
    """Izlušči rešitve iz datoteke in jih napiše v novo datoteko"""
    poglavje, koncnica = os.path.splitext(tex_datoteka)
    with open(tex_datoteka, encoding="utf-8") as fp:
        source = fp.read()
    # source = KOMENTAR_RE.sub("", source) To bo uredil tex. Problem je, ker pobere tudi \%
    ime = CHAPTER_RE.findall(source)[0]
    naloge = NALOGA_RE.findall(source)
    with open("{0}.resitve".format(poglavje), "w", encoding="utf-8") as fp:
        fp.write("\\chapter{{Rešitve: {0}}}\n".format(ime))
        for naloga in naloge:
            label = LABEL_RE.findall(naloga)[0]
            resitev = RESITEV_RE.findall(naloga)
            if len(resitev)>0:
                fp.write(RESITEV_TPL.format(label=label, resitev=resitev[0]))
            else:
                fp.write("\\addtocounter{resitevthm}{1}\n")
